/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Image,
  Text,
  ListView,
  View
} from 'react-native';

var MOCKED_PLAYERS_DATA = [
  {name: 'Devakumar', age: '1994'},
  ];

class CrickApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: null,
    };
  }

  render() {
    if (!this.state.players) {
      return this.renderLoadingView();
    }

    var player = MOCKED_PLAYERS_DATA[0];
    return this.renderPlayer(player);

  }

  renderLoadingView() {
    return (
      <View style={styles.container}>
        <Text>
          Loading list...
        </Text>
      </View>
    );
  }
  renderPlayer(player){
    return (
      <View style={styles.container}>
        <Text>{player.name}</Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  thumbnail: {
    width: 53,
    height: 81,
  },

});

AppRegistry.registerComponent('CrickApp', () => CrickApp);